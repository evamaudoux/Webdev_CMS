<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', 'C:\wamp64\www\wordpress\wp-content\plugins\wp-super-cache/' );
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'VKj/wU4De[SIOT,c?avlYi>%WcfRZ2oD5moSj+N}7EhkJv2 ~d//yS4CT>;]$FyZ' );
define( 'SECURE_AUTH_KEY',  '2A.aDw|~?+3@_h?`7X[|3hX})=`e0lg;i}-e S<Ksy+E|9op7Kk/N<NY$X]*KbqA' );
define( 'LOGGED_IN_KEY',    'p;.)2}Qw>x[VN/<#.iu0,bMOH.D:8Nnv#qLJ`[yb=-kLqq0FDlpNC9iZa)nu?*#0' );
define( 'NONCE_KEY',        '7EcU<sT!(U-/(GaKCp7-rUm._99!.l:v[zNN, Qr~/Y7^#W;0@19$tPp<t13]pbu' );
define( 'AUTH_SALT',        'bF){op)5ShgQvlc&Eu{k)?pL1b}{}a$X4,QE{l2ssTq(D7(9$#Q5pvTl{%b;b$/I' );
define( 'SECURE_AUTH_SALT', 'Z<n&ewn1o^ccO)WFWe-~[b3[e:7XuL~Zd2/j$7@_f}xfC4>9d.E~kWe$}(<Xu7oR' );
define( 'LOGGED_IN_SALT',   'iYDqOMzgy{ESA#qzS1|tM<Pb2s@<MW@[sukXv^:*.s}vD)}!0@BD27k;=V7<v4p|' );
define( 'NONCE_SALT',       'K57@*GRp.f1u%%42}Tq@!ibeG2.jEcVm?(94=RG*qRIkp_jJ,>.sE7PvyOVbZl9j' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
