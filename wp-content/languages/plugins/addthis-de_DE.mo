��          t      �              j        �     �     �     �     �     �  &   �       C  )     m  �  �     !     :     N     _  	   f     p  )   �     �                                            	            
    Privacy Policy Set up sharing tools from AddThis right in WordPress. With AddThis you can see how your users are engaging with your content, provide a personalized experience for each user and encourage them to share your content in their social networks. Follow tools, recommended content, conversion tools and social analytics available to registered accounts at addthis.com. Share Buttons by AddThis Terms of Service The AddThis Team Title:  Tool:  http://www.addthis.com https://wordpress.org/plugins/addthis/ the plugin's settings PO-Revision-Date: 2021-05-16 17:58:26+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: de
Project-Id-Version: Plugins - WordPress Share Buttons Plugin – AddThis - Stable (latest release)
 Datenschutzerklärung Richte die Teilen-Werkzeuge von AddThis direkt in WordPress ein. Mit AddThis kannst du sehen, wie deine Benutzer mit deinen Inhalten interagieren, ein personalisiertes Erlebnis für jeden Nutzer bieten und sie dazu ermutigen, deine Inhalte in den sozialen Netzwerken zu teilen. Folgen-Werkzeuge, empfohlene Inhalte, Conversion-Tools und Social Analytics sind für registrierte Konten unter addthis.com verfügbar. Share Buttons by AddThis Nutzungsbedingungen Das AddThis Team Titel: Werkzeug: http://www.addthis.com https://de.wordpress.org/plugins/addthis/ die Einstellungen des Plugins 