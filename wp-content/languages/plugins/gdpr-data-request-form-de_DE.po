# Translation of Plugins - GDPR Data Request Form - Stable (latest release) in German
# This file is distributed under the same license as the Plugins - GDPR Data Request Form - Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2018-05-24 06:08:52+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/3.0.0-alpha.2\n"
"Language: de\n"
"Project-Id-Version: Plugins - GDPR Data Request Form - Stable (latest release)\n"

#: public/hook.php:34
msgid "Select your request:"
msgstr "Wähle eine Anfrage aus:"

#. Author URI of the plugin
msgid "https://jeanbaptisteaudras.com"
msgstr "https://jeanbaptisteaudras.com"

#. Plugin URI of the plugin
msgid "https://jeanbaptisteaudras.com/gdpr-data-request-form"
msgstr "https://jeanbaptisteaudras.com/gdpr-data-request-form"

#. Plugin Name of the plugin
msgid "GDPR Data Request Form"
msgstr "GDPR Data Request Form"

#: public/hook.php:37
msgid "Your email address (required)"
msgstr "Deine E-Mail-Adresse (erforderlich)"

#: public/enqueues.php:25 public/public.php:25
msgid "Some errors occurred:"
msgstr "Einige Fehler sind aufgetreten:"

#: public/hook.php:36
msgid "Remove Personal Data"
msgstr "Personenbezogene Daten löschen"

#: public/hook.php:35
msgid "Export Personal Data"
msgstr "Personenbezogene Daten exportieren"

#: includes/data-request.php:43 data-request.php:43
msgid "All fields are required."
msgstr "Alle Felder sind erforderlich."

#: includes/data-request.php:50 data-request.php:50
msgid "Unable to initiate confirmation request. Please contact the administrator."
msgstr "Einwilligungs-Anfrage kann nicht ausgeführt werden. Bitte den Administrator kontaktieren."

#: includes/data-request.php:33 data-request.php:33
msgid "This is not a valid email address."
msgstr "Dies ist keine gültige E-Mail-Adresse."

#: includes/data-request.php:24 data-request.php:24
msgid "The request can’t be processed on this website. This feature requires WordPress 4.9.6 at least."
msgstr "Die Anfrage kann auf dieser Website nicht ausgeführt werden. Diese Funktion erfordert WordPress 4.9.6 oder höher."

#: public/widget.php:64
msgid "Optional widget description:"
msgstr "Optionale Widget-Beschreibung:"

#: public/widget.php:60
msgid "Optional widget title:"
msgstr "Optionaler Widget-Titel:"

#: public/enqueues.php:24 public/public.php:24
msgid "Your enquiry have been submitted. Check your email to validate your data request."
msgstr "Deine Anfrage wurde übermittelt. Prüfe deine E-Mails, um deine Daten-Anfrage zu bestätigen."

#: includes/data-request.php:39 data-request.php:39
msgid "Request type invalid, please refresh this page and try to submit the form again."
msgstr "Ungültiger Anfragetyp, bitte diese Seite neu laden und erneut versuchen, das Formular zu senden."

#: includes/data-request.php:30 data-request.php:30
msgid "Security check failed, please refresh this page and try to submit the form again."
msgstr "Sicherheitsprüfung fehlgeschlagen, bitte diese Seite neu laden und erneut versuchen, das Formular zu senden."