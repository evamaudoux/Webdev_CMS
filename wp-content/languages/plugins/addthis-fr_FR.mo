��          t      �              j        �     �     �     �     �     �  &   �       B  )     l  �  �     W     p     �  	   �  	   �     �  &   �     �                                            	            
    Privacy Policy Set up sharing tools from AddThis right in WordPress. With AddThis you can see how your users are engaging with your content, provide a personalized experience for each user and encourage them to share your content in their social networks. Follow tools, recommended content, conversion tools and social analytics available to registered accounts at addthis.com. Share Buttons by AddThis Terms of Service The AddThis Team Title:  Tool:  http://www.addthis.com https://wordpress.org/plugins/addthis/ the plugin's settings PO-Revision-Date: 2021-09-07 06:31:12+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fr
Project-Id-Version: Plugins - WordPress Share Buttons Plugin – AddThis - Stable (latest release)
 Politique de confidentialité Installer les outils de partage depuis AddThis directement dans WordPress. Avec AddThis, vous pouvez voir comment vos utilisateurs s’engagent avec votre contenu, fournir une expérience personnalisée pour chaque utilisateur et les encourager à partager votre contenu dans leurs réseaux sociaux. Outils de suivi, contenu recommandé, outils de conversion et statistiques des réseaux sociaux sont disponibles pour les utilisateurs inscrits sur addthis.com. Share Buttons by AddThis Conditions d’utilisation L’équipe AddThis Titre :  Outil :  http://www.addthis.com https://wordpress.org/plugins/addthis/ Les réglages de l’extension 